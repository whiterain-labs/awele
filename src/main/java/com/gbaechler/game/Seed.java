package com.gbaechler.game;

import org.apache.commons.lang3.math.NumberUtils;
import lombok.Getter;

@Getter
public class Seed {
	int weight = NumberUtils.INTEGER_ZERO;
}
